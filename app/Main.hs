module Main where

import TextProcessor

main :: IO ()
main = do
  contents <- processText <$> readFile "assembly/Pong.asm"
  writeFile "./machine/Pong.hack" contents