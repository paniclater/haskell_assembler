module Syntax
  (predefinedSymbols
  , Address(Address)
  , Instruction(..)
  , InstructionAddressMap(InstructionAddressMap)
  , Computation(Computation)
  , Destination(Destination)
  , Jump(Jump)
  ) where

import Text.Parsec
import qualified Data.Map.Strict as StrictMap
import Control.Arrow (second)

newtype Destination = Destination String deriving (Show)
newtype Computation = Computation String deriving (Show)
newtype Jump = Jump String deriving (Show)

data Instruction =
  C Destination Computation Jump -- C @D M+A JMP and jmp is just a thing
  | Location String -- @1010101010101010
  | Symbol String -- @BEGIN_ITERATION
  | Loop String -- (LOOP)
  | Comment -- //Comment
  | EmptyLine
  | SyntaxError ParseError deriving (Show)


newtype Address = Address Integer deriving (Show)
newtype InstructionAddressMap = InstructionAddressMap (StrictMap.Map String Address) deriving (Show)

predefinedSymbols :: InstructionAddressMap
predefinedSymbols = InstructionAddressMap $ StrictMap.fromList $ second Address <$> [
  ("SP", 0)
  , ("LCL", 1)
  , ("ARG", 2)
  , ("THIS", 3)
  , ("THAT", 4)
  , ("R0", 0)
  , ("R1", 1)
  , ("R2", 2)
  , ("R3", 3)
  , ("R4", 4)
  , ("R5", 5)
  , ("R6", 6)
  , ("R7", 7)
  , ("R8", 8)
  , ("R9", 9)
  , ("R10", 10)
  , ("R11", 11)
  , ("R12", 12)
  , ("R13", 13)
  , ("R14", 14)
  , ("R15", 15)
  , ("SCREEN", 16384)
  , ("KBD", 24576)
  ]