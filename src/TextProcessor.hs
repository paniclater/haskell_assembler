module TextProcessor where

import qualified Data.Map.Strict as StrictMap

import AssemblerParser (runParse)
import Evaluator (evaluate)
import Syntax (Instruction(..), InstructionAddressMap(..), Address(..), predefinedSymbols)

getMemoryLocation :: ([Instruction], InstructionAddressMap, Address) -> Instruction -> ([Instruction], InstructionAddressMap, Address)
getMemoryLocation (instructions, InstructionAddressMap withAddresses, (Address nextAddress)) (Symbol key) =
  let maybeSymbol = StrictMap.lookup key withAddresses
  in case maybeSymbol of
      Just existingAddress ->
        ((Location $ show existingAddress):instructions
        , InstructionAddressMap withAddresses
        , existingAddress)
      Nothing ->
        ((Location $ show nextAddress):instructions
        , InstructionAddressMap (StrictMap.insert key (Address nextAddress) withAddresses)
        , Address (nextAddress + 1))
getMemoryLocation (instructions, symbolMap, address) i = (i:instructions, symbolMap, address)

addLoopSymbol :: (InstructionAddressMap, Address) -> Instruction -> (InstructionAddressMap, Address)
addLoopSymbol (InstructionAddressMap symbols, Address count) (Loop l) =
  (InstructionAddressMap (StrictMap.insert l (Address (count + 1)) symbols), Address count)
addLoopSymbol (symbols, Address count) C{} = (symbols, Address (count + 1))
addLoopSymbol (symbols, Address count) (Location _) = (symbols, Address (count + 1))
addLoopSymbol (symbols, Address count) (Symbol _) = (symbols, Address (count + 1))
addLoopSymbol (symbols, count) _ = (symbols, count)

foldInstructionsWithMemoryAddress :: InstructionAddressMap -> [Instruction] -> ([Instruction], InstructionAddressMap, Address)
foldInstructionsWithMemoryAddress  s = foldl getMemoryLocation ([], s, Address 16)

processText :: String -> String
processText t =
  let
    parsedLines = map runParse $ lines t
    (symbols, _) = foldl addLoopSymbol (predefinedSymbols, Address (-1)) parsedLines
    addSymbols = foldInstructionsWithMemoryAddress symbols
    (instructionsWithAddresses, _,  _) = addSymbols parsedLines
  in
    unlines . reverse . filter (/= "") $ evaluate <$> instructionsWithAddresses