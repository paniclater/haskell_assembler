module Seven where

getDigit p x = z
  where (y, _) = x `divMod` (10 ^ p)
        (_, z) = y `divMod` 10

