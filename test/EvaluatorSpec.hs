module EvaluatorSpec where

import Evaluator
import Test.Hspec
import Test.QuickCheck

lengthCheck x =
  case to16BitBinaryString (x :: Int) of
    Just b -> length b == 16
    Nothing -> True

spec :: Spec
spec =
  it "should always produce a string of length 16" $
    property lengthCheck
